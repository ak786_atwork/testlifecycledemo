package com.example.testlifecycledemo

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


class TestFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        retainInstance = true
        showLog("oncreate")

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        showLog("oncreateview")

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_test, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        showLog("onattach called")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        showLog("onactivitycreated")
    }

    override fun onStart() {
        super.onStart()
        showLog("on start")
    }

    override fun onResume() {
        super.onResume()
        showLog("onresume")
    }

    override fun onPause() {
        super.onPause()
        showLog("onpause")
    }

    override fun onStop() {
        super.onStop()
        showLog("onstop")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        showLog("ondestroyview")
    }

    override fun onDestroy() {
        super.onDestroy()

        showLog("ondestroy")
    }


    override fun onDetach() {
        super.onDetach()

        showLog("ondetach")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        showLog("onsaveinstance")
    }

    private fun showLog(info: String) {
        Log.d("FRAGMENT-", info)
    }


}
