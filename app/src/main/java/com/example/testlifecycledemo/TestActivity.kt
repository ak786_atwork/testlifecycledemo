package com.example.testlifecycledemo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_test.*

class TestActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)



        click.setOnClickListener{
            launchActivity()
        }
    }
    private fun launchActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)


    }
}
