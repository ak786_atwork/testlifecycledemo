package com.example.testlifecycledemo

import android.content.Intent
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import androidx.lifecycle.ViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        showLog("oncreate called")
//        finish()          //it will directly ondestroy


        click.setOnClickListener{
            launchSecondActivity()
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        showLog("on post create")
    }


    private fun launchSecondActivity() {
        val intent = Intent(this, TestActivity::class.java)
        startActivity(intent)
    }


    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        showLog("on new intent called")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        showLog("on activity result")
    }

    override fun onStart() {
        super.onStart()
        showLog("onstart")
    }

    override fun onRestart() {
        super.onRestart()
        showLog("on restart")
    }

    override fun onResume() {
        super.onResume()
        showLog("onresume")
    }

    override fun onPause() {
        super.onPause()
        showLog("on pause")
    }

    override fun onStop() {
        showLog("onstop called")


      /*  val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.add(TestFragment(), "TestFragment")
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commitNow()        //java.lang.IllegalStateException
//        fragmentTransaction.commit()        //java.lang.IllegalStateException


        supportFragmentManager.executePendingTransactions()

        fragmentTransaction.add(TestFragment(), "TestFragment")
        fragmentTransaction.addToBackStack(null)*/

        super.onStop()

    }

    override fun onSaveInstanceState(outState: Bundle) {
        showLog("onsaveinstance called")
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        showLog("onrestore instance")
    }

    override fun onDestroy() {
        super.onDestroy()
        showLog("ondestroy")
    }

    private fun showLog(info: String) {
        Log.d("ACTIVITY- ", info)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        showLog("onconfiguration changed")
    }


}
